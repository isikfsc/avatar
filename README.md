# avatar
simple circle avatars

es6+jsx->normal

`babel -w index.jsx -o index.js`


require()s->normal

`browserify index.js -o bundle.js`

`watchify index.js -o bundle.js`


es6+jsx+require()s->normal

`browserify -t babelify index.jsx -o bundle.js`

`watchify -t babelify index.jsx -o bundle.js`


static sites

`browser-sync start --server --files "css/*.css"`


dynamic sites

`browser-sync start --proxy "myproject.dev" --files "css/*.css"`
